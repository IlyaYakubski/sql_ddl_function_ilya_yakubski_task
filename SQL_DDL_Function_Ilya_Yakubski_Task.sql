-- 1

CREATE VIEW sales_revenue_by_category_qtr AS
SELECT
    fc.Category_ID,
    c.Name AS Category_Name,
    SUM(p.Amount) AS Total_Sales_Revenue
FROM
    Film_Category fc
JOIN Film f ON fc.Film_ID = f.Film_ID
JOIN Inventory i ON f.Film_ID = i.Film_ID
JOIN Rental r ON i.Inventory_ID = r.Inventory_ID
JOIN Payment p ON r.Rental_ID = p.Rental_ID
JOIN Category c ON fc.Category_ID = c.Category_ID
WHERE
    DATE_PART('quarter', CURRENT_DATE) = DATE_PART('quarter', r.Rental_date)
GROUP BY
    fc.Category_ID, c.Name;


-- 2

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter integer)
RETURNS TABLE (
    category_name varchar,
    total_sales numeric
) AS $$
BEGIN
    RETURN QUERY
    SELECT
        c.name AS category_name,
        COALESCE(SUM(p.amount), 0) AS total_sales
    FROM
        category c
    LEFT JOIN
        film_category fc ON c.category_id = fc.category_id
    LEFT JOIN
        film f ON fc.film_id = f.film_id
    LEFT JOIN
        inventory inv ON f.film_id = inv.film_id
    LEFT JOIN
        rental r ON inv.inventory_id = r.inventory_id
    LEFT JOIN
        payment p ON r.rental_id = p.rental_id
    WHERE
        EXTRACT(QUARTER FROM r.rental_date) = current_quarter
    GROUP BY
        c.name
    ORDER BY
        total_sales DESC;

END;
$$ LANGUAGE plpgsql;


-- 3

CREATE OR REPLACE FUNCTION new_movie(title_param VARCHAR)
RETURNS VOID AS $$
DECLARE
    new_film_id INTEGER;
    lang_id INTEGER;
BEGIN
    SELECT MAX(film_id) + 1 INTO new_film_id FROM film;

    SELECT language_id INTO lang_id FROM language WHERE name = 'Klingon';

    IF lang_id IS NULL THEN
        INSERT INTO language (name, last_update) VALUES ('Klingon', CURRENT_TIMESTAMP);
        SELECT language_id INTO lang_id FROM language WHERE name = 'Klingon';
    END IF;

    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id, last_update)
    VALUES (new_film_id, title_param, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), lang_id, CURRENT_TIMESTAMP);

END;
$$ LANGUAGE plpgsql;